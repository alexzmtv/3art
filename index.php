<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<meta property = "og:title" content = "3ARt" />
	<meta property = "og:description" content = "Цивилизации разных эпох" />
    <meta content="/images/social_imagel.jpg" property="og:image">
    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/images/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/images/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
	<link rel="manifest" href="/images/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="/images/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<title>Развитие человечества</title>
	<link href="/css/reset.css" rel="stylesheet" />
	<link href="/css/main.css" rel="stylesheet" />
	<link href="/css/fonts.css" rel="stylesheet" />
	<link href="/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
</head>
<body>
<div class="open"></div>
<div class="wrapper">
<main role="main" class="js-slider">
<? include('connectdb.php'); 

function About($id) {
	global $mysql;
	$info=$mysql->query("SELECT about FROM `events` WHERE `ID`=$id ORDER BY ID")->fetch_assoc();
	echo $info['about'];
}
function Sub($id) {
	global $mysql;
	$info=$mysql->query("SELECT Sub_Event FROM `events` WHERE `ID`=$id ORDER BY ID")->fetch_assoc();
	foreach(unserialize($info['Sub_Event']) as $key => $value) {
		$rew=$mysql->query("SELECT About FROM `sub_event` WHERE `ID`=$value ORDER BY ID")->fetch_assoc();
		echo '<div class="container-item  js-container-item">
				<img class="sub_main" src="/images/SUB_'.$value.'.png">
				<p>';
		echo $rew['About'];
		echo '</p>
		</div>';
	}
}
?>
		<div class=" slide js-slide mCustomScrollbar">
			<div>
				<h1>Первобытное<br><span class="first">общество</span><br><span class="last">(2,5-1млн. - 4-3тыс. до н.э.)</span></h1>
				<img class="main" src="/images/1.png">
					<p class="main">
					<? 
						About(1);
					?>
					</p>
				<br />
				<div class="container">
				<? 
					Sub(1);
				?>
				</div>
			</div>
		</div>

		<div class=" slide js-slide mCustomScrollbar">
			<div>
				<h1>Древний<br><span class="first">мир</span><br><span class="last">(с 3 тыс. лет до н.э. до 5 в.н.э.)</span></h1>
				<img class="main" src="/images/2.png">
					<p class="main">
						<? 
						About(2);
						?>
					</p>
				<br />
				<div class="container">
					<? 
					Sub(2);
					?>
				</div>
			</div>
		</div>

		<div class=" slide js-slide mCustomScrollbar">
			<div>
				<h1>Средние<br><span class="first">века</span><br><span class="last">(с 5 в. до 15 в.)</span></h1>
				<img class="main" src="/images/3.png">
					<p class="main">
						<? 
						About(3);
						?>
					</p>
				<br />
				<div class="container">
					<? 
					Sub(3);
					?>
				</div>
			</div>
		</div>

		<div class=" slide js-slide mCustomScrollbar">
			<div>
				<h1>Новое<br><span class="first">время</span><br><span class="last">(с 15 в. до нач. 20 в.)</span></h1>
				<img class="main" src="/images/4.png">
					<p class="main">
						<? 
						About(4);
						?>
					</p>
				<br />
				<div class="container">
					<? 
					Sub(4);
					?>
				</div>
			</div>
		</div>

		<div class=" slide js-slide mCustomScrollbar">
			<div>
				<h1>Новейшее<br><span class="first">время</span><br><span class="last">(с нач. 20 в. по настоящее время)</span></h1>
				<img class="main" src="/images/5.png">
					<p class="main">
						<? 
						About(5);
						?>
					</p>
				<br />
				<div class="container">
					<? 
					Sub(5);
					?>
				</div>
			</div>
		</div>

</main>
<aside role="complementary">
	<img class="logo js-logo" src="/images/logo.png">
	
	<div class="popup js-popup-logo logo-back">

		<div class="cross js-close">
			
		</div>
	</div>
<div class="share"></div>
	<nav>
	  <ul>
		<li class="js-list" data-backmain="/images/1_Pervobytnoe_obschestvo.jpg">Первобытное общество</li>
		<li class="js-list" data-backmain="/images/2_Drevniy_mir.jpg">Древний мир</li>
		<li class="js-list" data-backmain="/images/3_Srednie_veka.jpg">Средние века</li>
		<li class="js-list" data-backmain="/images/4_Novoe_vremya.jpg">Новое время</li>
		<li class="js-list" data-backmain="/images/5_Noveyshee_vremya.jpg">Новейшее время</li>
	  </ul>
	</nav>
</aside>
</div>
<? include "box.php"; ?>
<script type="text/javascript" src="/js/jquery-1.12.2.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/js/scroll.js"></script>
<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>
<div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus"></div>
</body>
</html>