<!DOCTYPE html>
<html lang="en">
	<div class="popup out js-popup" >
		<div class="image-preview js-cover">
			
		</div>
		
		<p class="text js-text">
			История первобытного общества охватывает период с момента возникновения человека 2,5—1 млн. лет назад до образования первых государств в Азии и Африке (рубеж 4—3 тыс. до н. э.).
		</p>
		<div class="cross js-close">
			
		</div>

		<div class="bolt-left js-bolt-left">
			
		</div>

		<div class="bolt-right js-bolt-right">
			
		</div>
	</div>
