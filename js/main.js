$(document).ready(function(){
	var image, text, curslide, index, backmain;
	function slider(){
		image = curslide.find('img').attr('src');
		text = curslide.find('p').text();
		$('.js-popup').find('.js-cover').css({'background-image':'url('+image+')'});
		$('.js-popup').find('.js-text').text(text);
		$('.js-popup').addClass('active');
		$('body').css({'overflow':'hidden'});
	}
	$('.js-container-item').on('click',function(){
		
		if($(this).prev().length>0 && $(this).next().length>0){
			$('.js-bolt-left, .js-bolt-right').show();
		}
		else if($(this).siblings().length>0 && $(this).prev().length>0){
			$('.js-bolt-right').hide();
			$('.js-bolt-left').show();
		}
		else if($(this).siblings().length>0 && $(this).next().length>0){
			$('.js-bolt-left').hide();
			$('.js-bolt-right').show();
		}
		curslide = $(this);
		
		slider();
	});
	$('.js-close').on('click',function(){
		$('.js-popup').removeClass('active');
		$('.js-popup-logo').removeClass('active');
	});
	
	$('.js-logo').on('click',function(){
		$('.js-popup-logo').addClass('active');
	});
	$('.js-bolt-left').on('click',function(){

		curslide = curslide.prev();
		slider();
		$('.js-bolt-right').show('fast');
		if(curslide.prev().length<1){
			$('.js-bolt-left').hide('fast');
		}
	})
	$('.js-bolt-right').on('click',function(){
		
		curslide = curslide.next();
		slider();
		$('.js-bolt-left').show('fast');

		if(curslide.next().length<1){
			$('.js-bolt-right').hide('fast');
		}		
	});
	$('.js-list').on('click',function(){
		$('.js-list').removeClass('active');
		backmain = $(this).data('backmain');
		index = $(this).addClass('active').index();
		$('body').css({'background-image':'url('+backmain+')'});
		$('.js-slide').eq(index).addClass('active').siblings().removeClass('active');

	});
	$(window).on('load',function(){
		$('body').addClass('active');
	});
	
});